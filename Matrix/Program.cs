﻿using System;

namespace Matrix
{
	class MainClass
	{
		private static int[,] Matrix;
		private static int n, k;
		private static string [] MenuItems = 
		{
			"Транспонирование матрицы", 
			"Умножение матрицы на число",
			"Получение обратной матрицы",
			"Определитель матрицы (2 или 3 порядка)"
		};

		public static void Main (string[] args)
		{
			Console.WriteLine ("Добро пожаловать в Matrix v1.0!\n");
			Menu ();
		} 

		public static void showMatrix () {
			for (int i = 0; i < n; i++) {
				for (int j = 0; j < k; j++) {
					Console.Write (Matrix[i,j] + "\t");
				}
				Console.WriteLine ("\n");
			}
		}

		public static void getMatrix () {
			Console.WriteLine ("Введите размер матрицы:");
			n = Convert.ToInt32 (Console.ReadLine ());
			k = Convert.ToInt32 (Console.ReadLine ());
			Matrix = new int[n, k];
			Console.WriteLine ("Введите матрицу:");
			for (int i = 0; i < n; i++) {
				for (int j = 0; j < k; j++) {
					Console.Write ("Элемент " + i + " строки " + j + " столбца: ");
					Matrix [i, j] = Convert.ToInt32 (Console.ReadLine ());
				}
			}
		}

		public static void Menu () {
			string Command;
			Console.WriteLine ("Меню:");
			for (int i = 0; i < MenuItems.Length; i++) {
				int NumberItem = i + 1;
				Console.WriteLine (NumberItem + ") " + MenuItems[i]);
			}
			Console.Write ("Введите номер команды: ");
			Command = Console.ReadLine();

			Transponse matrix = new Transponse ();
			Multiplication matrix2 = new Multiplication ();
			InverseMatrix matrix3 = new InverseMatrix ();
			Determinate matrix4 = new Determinate ();

			switch (Command) {
				case "1": 
					getMatrix ();

					matrix.setMatrix (Matrix, n, k);
					matrix.Transporate ();

					Matrix = matrix.getMatrix ();
					n = matrix.getNewN ();
					k = matrix.getNewK ();

					Console.WriteLine ("Транспонированная матрица");
					showMatrix ();
					break;
				case "2":
					getMatrix ();

					Console.Write ("Введите множитель:");
					int number = Convert.ToInt32 (Console.ReadLine ());
					
					matrix2.setMatrix (Matrix, n, k);
					matrix2.setNumber (number);
					matrix2.makeMultiplication ();
					Matrix = matrix2.getMatrix ();

					Console.WriteLine ("Результат");
					showMatrix ();
					break;
			case "3":
				getMatrix ();

				if (n <= 3) {
					if (n == k) {
						int determinate = matrix4.getDeterminate (Matrix, n);
						matrix3.setMatrix (Matrix, n);
						matrix3.makeMinorMatrix ();
						matrix3.makeCofactorsMatrix ();
						matrix.setMatrix (matrix3.getCofactorsMatrix (), n, n);
						matrix.Transporate ();
						Matrix = matrix.getMatrix ();

						Console.WriteLine ("Обратная матрица:\n1/(" + determinate + ") * ");
						showMatrix ();
					} else {
						Console.WriteLine ("Ошибка. Матрица должна быть квадратной.");
					}
				} else {
					Console.WriteLine ("Ошибка. Программа может найти обратную матрицу не больше третьего порядка.");
				}
				break;
			case "4":
				getMatrix ();
				if (n <= 3) {
					if (n == k) {
						Console.WriteLine ("Определитель равен " + matrix4.getDeterminate(Matrix, n));
					} else {
						Console.WriteLine ("Ошибка. Матрица должна быть квадратной.");
					}
				} else {
					Console.WriteLine ("Ошибка. Программа может посчитать определитель не больше третьего порядка.");
				}
				break;
			}
		}
	}

	class Transponse 
	{
		private int [,] Matrix;
		private int n, k;

		public Transponse () {
			n = 0;
			k = 0;
			Matrix = null;
		}

		public void Transporate () {
			int [,] TransponseMatrix = new int[k,n];
			for (int i = 0; i < n; i++) {
				for (int j = 0; j < k; j++) {
					TransponseMatrix [j, i] = Matrix [i, j];
				}
			}
			Matrix = TransponseMatrix;
		}

		public int [,] getMatrix () {
			return Matrix;
		}

		public int getNewN () {
			return k;
		}

		public int getNewK () {
			return n;
		}

		public void setMatrix (int [,] matrix, int m, int l) {
			Matrix = matrix;
			n = m;
			k = l;
		}
	}

	class Multiplication
	{
		private int [,] Matrix;
		private int number, n, k;

		public Multiplication () {
			Matrix = null;
			number = 0;
		}

		public void setMatrix (int [,] matrix, int m, int l) {
			Matrix = matrix;
			n = m;
			k = l;
		}

		public void setNumber (int Number) {
			number = Number;
		}

		public void makeMultiplication () {
			for (int i = 0; i < n; i++) {
				for (int j = 0; j < k; j++) {
					Matrix [i, j] *= number;
				}
			}
		}

		public int [,] getMatrix () {
			return Matrix;
		}
	}

	class Determinate 
	{
		public int getDeterminate (int [,] matrix, int m) {
			if (m == 2) {
				return ((matrix [0, 0] * matrix [1, 1]) - (matrix [0, 1] * matrix [1, 0]));
			} else if (m == 3) {
				return ((matrix [0, 0] * matrix [1, 1] * matrix [2, 2] + matrix [1, 0] * matrix [2, 1] * matrix [0, 2] + matrix [0, 1] * matrix [1, 2] * matrix [2, 0]) - (matrix [0, 2] * matrix [1, 1] * matrix [2, 0] + matrix [1, 0] * matrix [0, 1] * matrix [2, 2] + matrix [0, 0] * matrix [2, 1] * matrix [1, 2]));
			} else
				return 0;
		}
	}

	class InverseMatrix 
	{
		private int [,] Matrix, MinorMatrix, CofactorsMatrix;
		private int n;

		public void setMatrix (int [,] matrix, int m) {
			Matrix = matrix;
			n = m;
		}

		public void makeMinorMatrix () {
			if (n == 2) {
				MinorMatrix = new int[n,n];
				MinorMatrix [0, 0] = Matrix [1, 1];
				MinorMatrix [0, 1] = Matrix [1, 0];
				MinorMatrix [1, 0] = Matrix [0, 1];
				MinorMatrix [1, 1] = Matrix [0, 0];
			} else if (n == 3) {
				MinorMatrix = new int[n,n];
				for (int i = 0; i < n; i++) {
					for (int j = 0; j < n; j++) {
						int [] values = new int[4];
						int index = 0;
						for (int x = 0; x < n; x++) {
							for (int y = 0; y < n; y++) {
								if (x != i && y != j) {
									values [index] = Matrix [x, y];
									index++;
								}
							}
						}
						index = (values [0] * values [3]) - (values [1] * values [2]);
						MinorMatrix [i, j] = index;
					}
				}
			}
		}

		public int [,] getMinorMatrix () {
			return MinorMatrix;
		}

		public void makeCofactorsMatrix () {
			CofactorsMatrix = new int[n,n];
			for (int i = 0; i < n; i++) {
				for (int j = 0; j < n; j++) {
					if (Math.Pow (-1, i + j + 2) < 0) {
						CofactorsMatrix [i, j] = MinorMatrix [i, j] * (-1);
					} else {
						CofactorsMatrix [i, j] = MinorMatrix [i, j];
					}
				}
			}
		}

		public int [,] getCofactorsMatrix () {
			return CofactorsMatrix;
		}
	}
}
